package com.addcel.api.talento.beans;

public class PurchaseBean {
	
	private String operation = "";
	private String result = "";
	private String message = "";
	private String folio = "";	
	
	public PurchaseBean() {
		super();
	}
	
	public PurchaseBean(String operation, String result, String message,
			String folio) {
		super();
		this.operation = operation;
		this.result = result;
		this.message = message;
		this.folio = folio;
	}
	
	public String getOperation() {
		return operation;
	}
	
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	public String getResult() {
		return result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getFolio() {
		return folio;
	}
	
	public void setFolio(String folio) {
		this.folio = folio;
	}

}
