package com.addcel.api.talento.beans;

public class ToDo {

	private String toDo;
	private Login login;
	private Purchase purchase;
	private Register register;
	private LoginUpdater loginUpdater;


	public String getToDo() {
		return toDo;
	}
	public void setToDo(String toDo) {
		this.toDo = toDo;
	}
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	public Purchase getPurchase() {
		return purchase;
	}
	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}
	public Register getRegister() {
		return register;
	}
	public void setRegister(Register register) {
		this.register = register;
	}
	public LoginUpdater getLoginUpdater() {
		return loginUpdater;
	}
	public void setLoginUpdater(LoginUpdater loginUpdater) {
		this.loginUpdater = loginUpdater;
	}
}
