package com.addcel.api.talento.utils;


import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Sys {

	public static final boolean DEPURANDO = false;
	private static final boolean LOG_TO_FILE = false;
	
	public static void log(String tag, String texto){
		if (DEPURANDO){
			Log.i(tag, texto);
			
			if (LOG_TO_FILE){
				LogUtils.logToFile("<" + tag + ">:\t\t" + texto);
			}
		}
	}
	
	
	public static void log(Exception e){
		if (DEPURANDO){
			e.printStackTrace();
			
			if (LOG_TO_FILE){
				LogUtils.logToFile(e);
			}
		}
	}
}
