package com.addcel.api.talento.utils;


public class Crypto {
	
	public static String aesEncrypt(String seed, String cleartext) {
		
		String encryptedText = null;
		
		try{
			
			encryptedText= AESBinFormat.encode(Utils.replaceConAcento(cleartext), seed);
			
		}catch(Exception e){
			encryptedText = "";
		}
		 	   
		return encryptedText;
	}
	
	
	public static String aesDecrypt(String seed, String encrypted) {
		String decryptedText;
		
		try{
			return Utils.reemplazaHTML(AESBinFormat.decode(encrypted, seed));
		}catch(Exception e){
			decryptedText = "";
		}
		
		return decryptedText;
	}
}