package com.addcel.api.talento.utils;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;




public class UtilBB {

	public static final int DEVICE_NAME = 0;
	public static final int SOFTWARE_VERSION = 1;
	public static final int MANUFACTURER_NAME = 2;

	
	public static String getIMEI(Context ctx){

			String imei = null;
			
			try{
				imei = ((TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
				
				if (imei == null){
					imei = Settings.System.getString(ctx.getContentResolver(), Settings.System.ANDROID_ID);
				}
			}catch(Exception e){
				Sys.log(e);
			}
			
		return "358987010088552";
	}

	
	
	public static String getDeviceINFO(int key) {

		String value = null;

		switch (key) {
		case DEVICE_NAME:
			value = Build.MODEL;
			break;

		case SOFTWARE_VERSION:
			value = Build.VERSION.RELEASE;
			break;

		case MANUFACTURER_NAME:
			value = Build.MANUFACTURER;
			break;
		}

		return value;
	}
}
