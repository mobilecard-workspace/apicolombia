package com.addcel.api.talento;

import org.json.JSONException;

import android.content.Context;

import com.addcel.api.talento.beans.GeneralBean;
import com.addcel.api.talento.beans.Login;
import com.addcel.api.talento.beans.LoginUpdater;
import com.addcel.api.talento.beans.Purchase;
import com.addcel.api.talento.beans.Register;
import com.addcel.api.talento.beans.ToDo;
import com.addcel.api.talento.thread.CreditInfoThread;
import com.addcel.api.talento.thread.LoginThread;
import com.addcel.api.talento.thread.PurchaseThread;
import com.addcel.api.talento.thread.RegisterThread;
import com.addcel.api.talento.thread.UpdateUserPassThread;
import com.addcel.api.talento.utils.Crypto;
import com.addcel.api.talento.utils.UtilBB;
import com.addcel.api.talento.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Executor {

	public static GeneralBean[] proveedores = null;
	public static GeneralBean[] bancos = null;
	public static GeneralBean[] tarjetas = null;
	public static GeneralBean[] estados = null;

	public static String sLogin = null;
	public static String sRegistro = null;
	public static String sVenta = null;
	public static String sLoginActualizado = null;
	private Gson gson;

	public Executor() {

		gson = gson = new GsonBuilder().disableHtmlEscaping().create();
	}

	public String creditInfo() throws JSONException {

		CreditInfoThread creditInfoThread = new CreditInfoThread("proveedores");
		creditInfoThread.run();

		String jBancos = gson.toJson(Executor.bancos);
		String jTarjetas = gson.toJson(Executor.tarjetas);
		String jEstados = gson.toJson(Executor.estados);
		String jProveedores = gson.toJson(Executor.proveedores);

		StringBuilder json = new StringBuilder();

		json.append("{");
		json.append("proveedores:").append(jProveedores).append(",");
		json.append("bancos:").append(jBancos).append(",");
		json.append("tarjetas:").append(jTarjetas).append(",");
		json.append("estados:").append(jEstados).append("");
		json.append("}");

		return json.toString();
	}

	public String login(final Login login, final Context context) throws JSONException {

		LoginThread loginThread = new LoginThread(login.getUsuario(), login.getContrasena(), context);
		loginThread.run();
		return sLogin;
	}

	public String createUserJson(String pass, String login) {

		String value = "{\"login\":\"" + login + "\", \"password\":\"" + pass
				+ "\"}";
		return value;
	}

	public String register(final Register register, final Context context) throws JSONException {

		String imei = null;
		String tipo = null;
		String software = null;
		String modelo = null;
		String key = null;

		imei = UtilBB.getIMEI(context);
		tipo = UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME);
		software = UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION);
		modelo = UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME);
		key = UtilBB.getIMEI(context);

		register.setBanco("1");
		register.setPassword("xCL8m39sJ1");
		register.setStatus("1");
		register.setTerminos("1");

		register.setImei(imei);
		register.setTipo(tipo);
		register.setSoftware(software);
		register.setModelo(modelo);
		register.setKey(key);

		String json = gson.toJson(register);

		String parsePass02 = Utils.parsePass(register.getPassword());
		String userJson02 = json;
		String n = Crypto.aesEncrypt(parsePass02, userJson02);
		String n2 = Utils.mergeStr(n, register.getPassword());

		RegisterThread registerThread = new RegisterThread(n2);
		registerThread.run();

		return sRegistro;
	}

	public String purchase(final ToDo toDo, final Context context) throws JSONException {

		String imei = UtilBB.getIMEI(context);
		String tipo = UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME);
		String software = UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION);
		String modelo = UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME);
		String key = UtilBB.getIMEI(context);

		Purchase purchase = toDo.getPurchase();

		String password = purchase.getPassword();

		purchase.setImei(imei);
		purchase.setTipo(tipo);
		purchase.setSoftware(software);
		purchase.setModelo(modelo);
		purchase.setKey(key);

		String js = gson.toJson(purchase);

		String jsonpass = Crypto.aesEncrypt(Utils.parsePass(password), js);
		String json = Utils.mergeStr(jsonpass, password);

		PurchaseThread purchaseThread = new PurchaseThread(json, password);
		purchaseThread.run();

		return sVenta;
	}

	
	public String loginUpdate(final ToDo toDo, final Context context) throws JSONException {

		LoginUpdater loginUpdater = toDo.getLoginUpdater();

		UpdateUserPassThread updateUserPassThread = new UpdateUserPassThread(loginUpdater.getLogin(), loginUpdater.getPassword(), loginUpdater.getNewPassword());
		updateUserPassThread.run();

		return sLoginActualizado;
	}
	
}
