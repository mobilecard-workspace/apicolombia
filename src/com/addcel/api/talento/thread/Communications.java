package com.addcel.api.talento.thread;

public class Communications {

	/**
	 * Clase estatica para las comunicaciones en cualquier Midlet y que asegura
	 * un unico hilo para conexion hacia el o los servidores
	 */

	/** Objeto HttpPoster unico y persistente durante toda la vida del midlet */
	protected static HttpPoster poster = null;

	/**
	 * Metodo que inicia el thred de comunicacion (no conexion) y es necasaria
	 * su invocacion antes de cualquier intento de conexion con el server
	 **/
	public static void start() {
		poster = new HttpPoster();
	}

	/**
	 * Metodo para iniciar una conexion con algun servidor
	 * 
	 * @param _listener
	 *            Objeto en el cual se va a depositar el resultado de la
	 *            peticion
	 * @param _url
	 *            Direccion a la cual se requiere la conexion
	 */
	public static void sendHttpRequest(HttpListener _listener, String _url) {
		poster.sendHttpRequest(_listener, _url);

	}

	/**
	 * Metodo para iniciar una conexion con algun servidor
	 * 
	 * @param _listener
	 *            Objeto en el cual se va a depositar el resultado de la
	 *            peticion
	 * @param _url
	 *            Direccion a la cual se requiere la conexion
	 * @param _tries
	 *            Numero de intentos de conexion
	 */
	public static void sendHttpRequest(HttpListener _listener, String _url,
			int _tries) {
		poster.setIntentos(_tries);
		poster.sendHttpRequest(_listener, _url);
	}

	/**
	 * Destructor del objeto de comunicaciones, es necesario su llamada para
	 * detener el hilo y liberar la memoria
	 */
/*
	public static void destroy() {
		poster.abort();
		poster.closeConnection();
		poster = null;
	}

	public static void reestart() {
		poster.abort();
		poster.closeConnection();
		poster = new HttpPoster();
	}

	public void abort() {
		poster.closeConnection();
	}
*/

	public static void sendHttpPost(HttpListener _listener, String _url,
			String _post) {
		poster.sendHttpRequest(_listener, _url, _post);
	}
}