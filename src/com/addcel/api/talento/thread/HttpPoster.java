package com.addcel.api.talento.thread;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;


//public class HttpPoster implements Runnable {
public class HttpPoster{
	//private ContentConnection connection = null;
	private volatile boolean aborting = false;
	private Vector listenerQueue = new Vector();
	private boolean post = false;
	private String stringPost = null;
	private Vector URLQueue = new Vector();
	private int intentos = 1;
	//private HttpConnection conn = null;
	private int flag = 0;

	private String url = "";
	private HttpListener listener = null;

	public HttpPoster() {
		//Thread thread = new Thread(this);
		//thread.start();
	}

	public synchronized void sendHttpRequest(HttpListener listener, String URL) {

		if (aborting == true) {
			aborting = false;
			this.run();
		}

		listenerQueue.addElement(listener);
		stringPost = null;
		post = false;
		URLQueue.addElement(URL);
		
		doSend(listener, URL);
		notify();
	}

	public synchronized void sendHttpRequest(HttpListener listener, String URL,
			String _post) {
		if (aborting == true) {
			aborting = false;
			this.run();
		}

		stringPost = _post;
		post = true;
		listenerQueue.addElement(listener);

		URLQueue.addElement(URL);
		doSend(listener, URL, stringPost);
		notify();
	}

	public void run() {
		running: while (!aborting) {
			url = "";
			synchronized (this) {
				while (listenerQueue.size() == 0) {
					try {
						wait(); // releases lock

					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (aborting) {
						break running;
					}
				}

				listener = (HttpListener) (listenerQueue.elementAt(0));
				url = (String) (URLQueue.elementAt(0));

				listenerQueue.removeElementAt(0);
				URLQueue.removeElementAt(0);
			}
			if (post)
				doSend(listener, url, stringPost);
			else
				doSend(listener, url);
		}
	}


	private void doSend(HttpListener listener, String urlString, String post) {

		int code = 0;
		DataInputStream in = null;
		OutputStream dos;
		String errorStr = null;
		boolean wasError = false;
		ByteArrayOutputStream responseBytes = null;
		HttpURLConnection connection = null;
		byte data[] = null;
		byte postByte[] = null;

		try {
			
			URL url = new URL(urlString); 
			URLConnection conn = url.openConnection();
			connection = (HttpURLConnection) conn;
			
			postByte = new byte[post.length()];
			postByte = post.getBytes();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("content-length", postByte.length
					+ "");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			dos = connection.getOutputStream();
			dos.write(postByte);
			dos.flush();
			dos.close();
			// poner post
			
			InputStream inputStream = connection.getInputStream();
			
			int length = inputStream.available();
			
			in = new DataInputStream(inputStream);
			
			if (length != -1) {
				data = new byte[length];

				// Read the png into an array
				in.readFully(data);
			} else {
				// Length not available...
				responseBytes = new ByteArrayOutputStream();
				int ch;
				while ((ch = in.read()) != -1)
					responseBytes.write(ch);

				data = responseBytes.toByteArray();
			}

		} catch (Exception e) {
			wasError = true;
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (wasError) {
			if (intentos >= 1) {
				if (!aborting) {
					if (listener != null && !listener.isDestroyed())
						listener.handleHttpError(code, errorStr);
				}
			} else {
				System.gc();
			}
		} else {

			try {
				if (listener != null && !listener.isDestroyed()) {
					listener.receiveHttpResponse(0, data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	
	private void doSend(HttpListener listener, String urlString) {
		int code = 0;
		InputStream in = null;
		String errorStr = null;
		boolean wasError = false;
		ByteArrayOutputStream responseBytes = null;
		HttpURLConnection connection = null;
		byte data[] = null;

		try {
			
			URL url = new URL(urlString); 
			URLConnection conn = url.openConnection();
			connection = (HttpURLConnection) conn;
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			InputStream stream = connection.getInputStream();

			int response = stream.available();

			Reader reader = null;
			reader = new InputStreamReader(stream);
			char[] buffer = new char[response];
			reader.read(buffer);
			String string =  new String(buffer);
			
			data = string.getBytes();

		} catch (Exception e) {
			wasError = true;
			e.printStackTrace();
		} finally {

			try {

				if (in != null) {
					in.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			if (connection != null) {
				connection.disconnect();
			}
		}

		if (wasError) {
			if (intentos >= 1) {
				if (!aborting) {
					if (listener != null && !listener.isDestroyed())
						listener.handleHttpError(code, errorStr);
				}
			} else {
				System.gc();
			}
		} else {

			try {
				if (listener != null && !listener.isDestroyed()) {
					listener.receiveHttpResponse(0, data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void abort() {

		listenerQueue.removeAllElements();
		URLQueue.removeAllElements();
		synchronized (this) {
			notify();
		}
	}

	public boolean isAborting() {
		return aborting;
	}

	public int getIntentos() {
		return intentos;
	}

	public void setIntentos(int value) {
		intentos = value;
	}

	public void SetFlag(int f) {
		flag = f;
	}

	public int GetFlag() {
		return flag;
	}
}
