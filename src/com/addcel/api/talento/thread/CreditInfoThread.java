package com.addcel.api.talento.thread;

import java.util.Vector;

import com.addcel.api.talento.Executor;
import com.addcel.api.talento.beans.GeneralBean;
import com.addcel.api.talento.utils.JSONParser;
import com.addcel.api.talento.utils.UtilSecurity;


public class CreditInfoThread implements HttpListener {

	public String url = "";
	public String id = "";
	public String key = "1234567890ABCDEF0123456789ABCDEF";
	
	public CreditInfoThread(String id) {

		this.id = id;

		if (this.id.equals("bancos")) {
			this.url = Url.URL_GET_BANKS;
		} else if (this.id.equals("tarjetas")) {
			this.url = Url.URL_GET_CARDTYPES;
		} else if (this.id.equals("proveedores")) {
			this.url = Url.URL_GET_PROVIDERS;
		} else if (this.id.equals("estados")) {
			this.url = Url.URL_GET_ESTADOS;
		}
	}

	public void run() {
		connect();
	}

	public void connect() {
		Communications.start();
		Communications.sendHttpRequest(this, this.url);
	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError();
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub

	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub

	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();

		try {

			sTemp = new String(response);
			
			GeneralBean[] gralBeans = null;
			sTemp = UtilSecurity.aesDecrypt(key, sTemp);

			gralBeans = JSONParser.getInfoCredits(this.id, sTemp);

			if (gralBeans != null) {

				if (gralBeans.length > 0) {

					if (this.id.equals("proveedores")) {
						Executor.proveedores = gralBeans;
						new CreditInfoThread("bancos").run();
					} else if (this.id.equals("bancos")) {
						Executor.bancos = gralBeans;
						new CreditInfoThread("tarjetas").run();
					} else if (this.id.equals("tarjetas")) {
						Executor.tarjetas = gralBeans;
						new CreditInfoThread("estados").run();
					} else if (this.id.equals("estados")) {
						Executor.estados = gralBeans;
					}

				} else {
					getMessageError();
				}

			} else {

				getMessageError();
			}

		} catch (Exception e) {

			getMessageError();
		}
	}

	public void getMessageError() {
	}
}
