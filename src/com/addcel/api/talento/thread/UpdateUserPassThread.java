package com.addcel.api.talento.thread;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import com.addcel.api.talento.Executor;
import com.addcel.api.talento.beans.UserBean;
import com.addcel.api.talento.utils.UtilSecurity;
import com.addcel.api.talento.utils.Utils;

//public class UpdateUserPassThread extends Thread implements HttpListener{
public class UpdateUserPassThread implements HttpListener{

	public String url = Url.URL_UPDATE_PASS_MAIL;
	public String post = "";

	
	private String login;
	private String password;
	private String newPassword;

	public UpdateUserPassThread(String login, String password, String newPassword){
		
		this.login = login;
		this.password = password;
		this.newPassword = newPassword;
		
		String pas = "";
		try {
			pas = UtilSecurity.aesEncrypt(UtilSecurity.parsePass(newPassword), updatePswd(login, password, newPassword));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		System.out.println(pas);
		
		String pass = UtilSecurity.mergeStr(pas, newPassword);
		System.out.println(pass);

		this.post = "json=" + pass;
	}
	
	
	public String updatePswd(String login, String password, String newPassword) throws UnsupportedEncodingException {

		StringBuffer jsonPswd = new StringBuffer("{\"login\":\"");
		jsonPswd.append(login);
		jsonPswd.append("\",\"passwordS\":\""+ UtilSecurity.sha1(password));
		jsonPswd.append("\",\"password\":\"" + newPassword
				+ "\",\"newPassword\":\"");
		jsonPswd.append(newPassword + "\"}");
		
		System.out.println(jsonPswd.toString());
		
		return jsonPswd.toString();
	}

	
	public void run() {
		connect();
	}
	
	
	public void connect() {

		if (this.url != null) {
			try {
				Communications.start();
				Communications.sendHttpPost(this, this.url, this.post);

			} catch (Exception t) {
				t.printStackTrace();
			}
		}

	}

	public boolean isDestroyed() {
		return false;
	}
	
	public void handleHttpError(int errorCode, String error) {}

	public void receiveEstatus(String msg) {}

	public void receiveHeaders(Vector _headers) {}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		sTemp = new String(response, 0, response.length);
		sTemp = UtilSecurity.aesDecrypt(Utils.parsePass(newPassword), sTemp);
		Executor.sLoginActualizado = sTemp;
	}
}

