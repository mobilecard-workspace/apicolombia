package com.addcel.api.talento.thread;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import com.addcel.api.talento.Executor;
import com.addcel.api.talento.utils.Crypto;
import com.addcel.api.talento.utils.Utils;

public class PurchaseThread implements HttpListener{
	
	public String url = Url.URL_PURCHASE;
	public String post = "";
	public String pass = "";
	

	public PurchaseThread(String json,String pass){
		
		this.post = "json="+json;
		this.pass=pass;
		
	}

	public void run() {
		connect();
	}
	
	public void connect(){
		
		Communications.start();
		Communications.sendHttpPost(this,this.url,this.post);
	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError();
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {}


	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());

			sTemp = Crypto.aesDecrypt(Utils.parsePass(pass), sTemp);

			Executor.sVenta = sTemp;

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			getMessageError();
		}
	}


	public void getMessageError(){

	}

	@Override
	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}
}
