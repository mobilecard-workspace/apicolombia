package com.addcel.api.talento.thread;

public class Url {

	public static String URL_GET_BANKS = "http://mobilecard.mx:8080/AddCelBridgeColombia/Servicios/adc_getBanks.jsp";
	public static String URL_GET_CARDTYPES = "http://mobilecard.mx:8080/AddCelBridgeColombia/Servicios/adc_getCardType.jsp";
	public static String URL_GET_PROVIDERS = "http://mobilecard.mx:8080/AddCelBridgeColombia/Servicios/adc_getProviders.jsp";
	public static String URL_GET_ESTADOS = "http://mobilecard.mx:8080/AddCelBridgeColombia/Servicios/adc_getEstados.jsp";
	public static String URL_USER_INSERT = "http://mobilecard.mx:8080/AddCelBridgeColombia/Servicios/adc_userInsert.jsp";
	public static String URL_LOGIN = "http://mobilecard.mx:8080/AddCelBridgeColombia/Servicios/adc_userLogin.jsp";
	public static String URL_PURCHASE = "http://mobilecard.mx:8080/AddCelBridgeColombia/Servicios/adc_purchaseVotes.jsp";
	public static String URL_UPDATE_PASS_MAIL = "http://mobilecard.mx:8080/AddCelBridgeColombia/Servicios/adc_userPasswordUpdateMail.jsp";
}
