package com.addcel.api.talento.thread;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import com.addcel.api.talento.Executor;
import com.addcel.api.talento.utils.UtilBB;
import com.addcel.api.talento.utils.UtilSecurity;

import android.content.Context;

public class LoginThread implements HttpListener {

	private String url = Url.URL_LOGIN;

	private String password = null;
	private String json = null;
	private String jsonEncrypt = null;
	private String jsonEncrypt2 = null;

	private String user = null;
	
	private String post;
	
	private Context context;

	
	public LoginThread(String user, String password, Context context) {

		this.context = context;
		setClass(user, password);
	}
	
	private void setClass(String user, String password){


		try {

			if ((user != null) && (password != null)) {

				user = user.trim();
				this.password = password.trim();
				json = createJSon(user, password);
				
				String parse = UtilSecurity.parsePass(password);
				jsonEncrypt = UtilSecurity.aesEncrypt(parse, json);
				jsonEncrypt2 = UtilSecurity.mergeStr(jsonEncrypt, password);
				this.post = "json=" + jsonEncrypt2;
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	
	private String createJSon(String user, String password)
			throws UnsupportedEncodingException {

		StringBuffer json = new StringBuffer();

		this.user = user;
		
		json.append("{\"login\":\"").append(user);
		json.append("\",\"tipo\":\"").append(UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
		json.append("\",\"imei\":\"").append(UtilBB.getIMEI(context));
		json.append("\",\"modelo\":\"").append(UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
		json.append("\",\"software\":\"").append(UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
		json.append("\",\"key\":\"").append(UtilBB.getIMEI(context));
		json.append("\",\"password\":\"").append(password);
		json.append("\",\"passwordS\":\"").append(UtilSecurity.sha1(password));
		json.append("\"}");

		return json.toString();
	}

	public void connect() {

		//Communications communicator = Communicator.getInstance();
		Communications.start();
		Communications.sendHttpPost(this, this.url, this.post);

	}

	public void run() {
		//this.setPriority(Thread.MAX_PRIORITY);
		connect();
	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError(error);
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {}

	public void receiveHeaders(Vector _headers) {}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();

		sb.append(new String(response, 0, response.length));
		sTemp = new String(sb.toString());
		sTemp = UtilSecurity.aesDecrypt(UtilSecurity.parsePass(password), sTemp);
		Executor.sLogin = sTemp;
	}

	public void getMessageError(String error) {

		final String errors = error;
	}
}
