package com.addcel.api.talento.thread;

import java.util.Vector;

import com.addcel.api.talento.Executor;
import com.addcel.api.talento.beans.UserBean;
import com.addcel.api.talento.utils.UtilSecurity;

public class RegisterThread implements HttpListener {

	private String url = Url.URL_USER_INSERT;
	private String post = "";

	public RegisterThread(String json) {

		this.post = "json=" + json;
	}

	public void run() {
		connect();
	}

	public void connect() {

		Communications.start();
		Communications.sendHttpPost(this, this.url, this.post);
	}

	public void handleHttpError(int errorCode, String error) {
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		try {

			sb.append(new String(response, 0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());

			sTemp = UtilSecurity.aesDecrypt(
					UtilSecurity.parsePass(UserBean.passTEMP), sTemp);

			Executor.sRegistro = sTemp;

		} catch (Exception e) {

			getMessageError();
		}
	}

	public void getMessageError() {}

}
